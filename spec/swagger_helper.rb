require 'rspec/rails/swagger'
require 'rails_helper'

RSpec.configure do |config|
  # Specify a root directory where the generated Swagger files will be saved.
  config.swagger_root = Rails.root.to_s + '/swagger'

  # Define one or more Swagger documents and global metadata for each.
  config.swagger_docs = {
      'v1/swagger.json' => {
          swagger: '2.0',
          info: {
              title: 'API V1',
              version: 'v1'
          },
          definitions: {
              user: {
                  type: :object,
                  properties: {
                      user: {
                          type: :object,
                          required: %i[first_name last_name salary],
                          properties: {
                              first_name: { type: :string, example: 'test1' },
                              last_name: { type: :string, example: 'test1' },
                              salary: { type: :integer, example: 10 },
                          }
                      }
                  }
              }
          }
      }
  }
end