FactoryBot.define do
  factory :user do
    sequence :first_name do |n|
      "test #{n}"
    end

    sequence :last_name do |n|
      "test #{n}"
    end

    salary { rand(10) }
  end
end