require 'rails_helper'

RSpec.describe 'Users', type: :request do
  let!(:user) { FactoryBot.create(:user) }
  before {
    post '/api/v1/users', params: { user: { first_name: user.first_name, last_name: user.last_name, salary: user.salary} }
  }

  it 'creates a User' do
    expect(response.content_type).to eq("application/json; charset=utf-8")
    expect(response).to have_http_status(:created)
  end
end